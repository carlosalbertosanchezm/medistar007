-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema medistarpharmacydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema medistarpharmacydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `medistarpharmacydb` DEFAULT CHARACTER SET utf8mb3 ;
USE `medistarpharmacydb` ;

-- -----------------------------------------------------
-- Table `medistarpharmacydb`.`personas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medistarpharmacydb`.`personas` (
  `IdPersona` INT NOT NULL AUTO_INCREMENT,
  `RolPersona` VARCHAR(20) NOT NULL,
  `nombrePersona` VARCHAR(50) NOT NULL,
  `ApellidoPersona` VARCHAR(50) NOT NULL,
  `UsuarioPersona` VARCHAR(20) NOT NULL,
  `ClavePersona` VARCHAR(20) NOT NULL,
  `TipoIdentificacion` VARCHAR(20) NOT NULL,
  `NumeroIndentificacion` VARCHAR(20) NOT NULL,
  `DireccionPersona` VARCHAR(100) NOT NULL,
  `CiudadPersona` VARCHAR(50) NOT NULL,
  `EmailPersona` VARCHAR(100) NOT NULL,
  `TelefonoPersona` VARCHAR(20) NOT NULL,
  `FechaLogging` DATETIME NOT NULL,
  PRIMARY KEY (`IdPersona`),
  UNIQUE INDEX `NumeroIndentificacion` (`NumeroIndentificacion` ASC) VISIBLE,
  UNIQUE INDEX `EmailPersona` (`EmailPersona` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 1014
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `medistarpharmacydb`.`facturas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medistarpharmacydb`.`facturas` (
  `IdFactura` INT NOT NULL AUTO_INCREMENT,
  `Fk_IdPersona` INT NOT NULL,
  `FacturaVenta` VARCHAR(20) NOT NULL,
  `ValorTotalFct` FLOAT NOT NULL,
  `FechaFactura` DATETIME NOT NULL,
  PRIMARY KEY (`IdFactura`),
  INDEX `Fk_IdPersona` (`Fk_IdPersona` ASC) VISIBLE,
  CONSTRAINT `facturas_ibfk_1`
    FOREIGN KEY (`Fk_IdPersona`)
    REFERENCES `medistarpharmacydb`.`personas` (`IdPersona`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `medistarpharmacydb`.`productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medistarpharmacydb`.`productos` (
  `IdProducto` INT NOT NULL AUTO_INCREMENT,
  `NombreProducto` VARCHAR(50) NOT NULL,
  `CategoriaProducto` VARCHAR(20) NOT NULL,
  `ValorUnitarioPrd` FLOAT NOT NULL,
  `CantidadInventario` INT NOT NULL,
  PRIMARY KEY (`IdProducto`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `medistarpharmacydb`.`transacciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medistarpharmacydb`.`transacciones` (
  `IdTransaccion` INT NOT NULL AUTO_INCREMENT,
  `Fk_IdFactura` INT NOT NULL,
  `Fk_IdProducto` INT NOT NULL,
  `CantidadSalida` INT NOT NULL,
  `ValorUnitarioVnt` FLOAT NOT NULL,
  PRIMARY KEY (`IdTransaccion`),
  INDEX `Fk_IdFactura` (`Fk_IdFactura` ASC) VISIBLE,
  INDEX `Fk_IdProducto` (`Fk_IdProducto` ASC) VISIBLE,
  CONSTRAINT `transacciones_ibfk_1`
    FOREIGN KEY (`Fk_IdFactura`)
    REFERENCES `medistarpharmacydb`.`facturas` (`IdFactura`),
  CONSTRAINT `transacciones_ibfk_2`
    FOREIGN KEY (`Fk_IdProducto`)
    REFERENCES `medistarpharmacydb`.`productos` (`IdProducto`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
