/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.example.MediStarF2.modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author XIOMARA
 */
@Entity
@Table(name="productos")
public class Producto implements Serializable {
    
    //Atributos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IdProducto")
    private Integer idProducto;
    
    @Column(name="NombreProducto")
    private String nombreProducto;
    
    @Column(name="valorUnitarioPrd")
    private double valorUnitario;
    
    @Column(name="CantidadInventario")
    private double cantidadInventario;

    //Metodos
    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public double getCantidadInventario() {
        return cantidadInventario;
    }

    public void setCantidadInventario(double cantidadInventario) {
        this.cantidadInventario = cantidadInventario;
    }
    
    
    
}
