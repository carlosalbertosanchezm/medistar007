/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 * farmaciaVirtual MediStartPharmacy
 * Esta clase permite hacer el manejo integral de las Personas de la Farmacia.
 * En este documento en espanol se trato de evitar ls tildes y las enes   hahaha
 *
 * Criterios de variables
 *   solo letras a excepción de las llaves foraneas
 * a) Base de datos
 *  nombres de las tablas en plural y minusculas
 *  nombres de los atributos de las tablas PascalCase, dos palabras completas, si se requiere, para las palabras adicionales
 *  solo las tres primeras consonantes, ejemplo atributo   valor Unitario de Compra =  ValorUnitarioCmp
 *  en todas las variables se indica a que tabla pertenecen con la ultima palabra.
 *  las llaves foraneas se denominan así   Fk_NameAtributte    donde NameAtributte es el nombre de la primary key de la tabla referenciada.
 * b)Clases
 *   todas en singular PascalCase  ejemplo clase Persona
 * c) variables en Java
 *  todas empiezan con Minusculas,  camelCase   ejemplo  valorUnitarioCmp
 *
 * @author Carlos Alberto Sánchez Medina
 *
 * Que encontrará en este archivo
 * 1. Package and imports
 * 2. @ arrobas
 * 3. Atributtes
 * 5. Getters and Setters
 * 6. ToString
 */
/*
  1. Package and imports
 */
package com.example.MediStarF2.modelos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/*
* 2. @ arrobas 
 */
// esta entidad siempre se deja pegada al nombre de la clase
// esto se necesita para indicar que la clase Persona es una entidad con la tabla "listPersonas" de la base de datos// 
@Entity
@Table(name = "personas")
public class Persona implements Serializable {  // es para de SQL 

//    @Autowired // autowire permite inyectar codigo Sql en java
//    transient JdbcTemplate jdbcTemplate;  // JdbcTemplate es una clase que permite ejecutar   las consulta sql
    // Puede que no siempre se ejecute la consulta.  
    // este "atributo" no debe hacer parte del constructor ni de los gettres ni stters ni del tostring

    /* 
* 3. Atributtes
     */
    @Id     // esto es unicamente para la llave primaria    primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY)    // esto es unicamente si la llave primaria es autoincremental
    @Column(name = "IdPersona")  // este "IdPersona" es el nombre del campo en la base de datos
    private int idPersona;    // este "idPersona"   es el nombre del atributo en Java Netbeans
    @Column(name = "RolPersona")
    private String rolPersona;
    @Column(name = "NombrePersona")
    private String nombrePersona;
    @Column(name = "ApellidoPersona")
    private String apellidoPersona;
    @Column(name = "UsuarioPersona")
    private String usuarioPersona;
    @Column(name = "ClavePersona")
    private String clavePersona;
    @Column(name = "TipoIdentificacion")
    private String tipoIdentificacion;
    @Column(name = "NumeroIdentificacion")
    private String numeroIdentificacion;
    @Column(name = "DireccionPersona")
    private String direccionPersona;
    @Column(name = "CiudadPersona")
    private String ciudadPersona;
    @Column(name = "EmailPersona")
    private String emailPersona;
    @Column(name = "TelefonoPersona")
    private String telefonoPersona;
    @Column(name = "FechaLogging")
    private Date fechaLogging; // que tipo de dato para fechas Date
// si hay una foreign key  entonces:
    // @ManyToOne     o @OneToOne  o 
    //JoinColumn(name="IdProducto"
    //private Producto Fk_IdProducto    quiere decir que es un atributo tipo Clase Producto llamado Fk_IdProducto en la tbal personas 
    // private Estudiante Fk_IdEstudiante  // es la forma de colocar la llave foranea  de la clase estudiante en el campo IdEstudiante

    /*
    * 4. Builders
     */
    // empty builder
 
    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getRolPersona() {
        return rolPersona;
    }

    public void setRolPersona(String rolPersona) {
        this.rolPersona = rolPersona;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public String getApellidoPersona() {
        return apellidoPersona;
    }

    public void setApellidoPersona(String apellidoPersona) {
        this.apellidoPersona = apellidoPersona;
    }

    public String getUsuarioPersona() {
        return usuarioPersona;
    }

    public void setUsuarioPersona(String usuarioPersona) {
        this.usuarioPersona = usuarioPersona;
    }

    public String getClavePersona() {
        return clavePersona;
    }

    public void setClavePersona(String clavePersona) {
        this.clavePersona = clavePersona;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getDireccionPersona() {
        return direccionPersona;
    }

    public void setDireccionPersona(String direccionPersona) {
        this.direccionPersona = direccionPersona;
    }

    public String getCiudadPersona() {
        return ciudadPersona;
    }

    public void setCiudadPersona(String ciudadPersona) {
        this.ciudadPersona = ciudadPersona;
    }

    public String getEmailPersona() {
        return emailPersona;
    }

    public void setEmailPersona(String emailPersona) {
        this.emailPersona = emailPersona;
    }

    public String getTelefonoPersona() {
        return telefonoPersona;
    }

    public void setTelefonoPersona(String telefonoPersona) {
        this.telefonoPersona = telefonoPersona;
    }

    public Date getFechaLogging() {
        return fechaLogging;
    }

    /*
     * 5. Getters and Setters
    nameAtributte= nombre del atributo ->   GETnameAtributte = mostrar o consultar el atributo y  SETnameAtributte = modificar el Atributte
    aqui no va el JdbcTemplate
    aqui se deben incluir las llaves foraneas en caso de existir
     */
    public void setFechaLogging(Date fechaLogging) {  
        this.fechaLogging = fechaLogging;
    }

    }
    
  