/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.MediStarF2.modelos;
import com.example.MediStarF2.modelos.Persona;
import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Felipe G
 */

@Entity
@Table(name="factura")
public class Factura implements Serializable {
 
 
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name="idfactura")
private Integer idFactura;

@ManyToOne
@JoinColumn(name="Fk_IdPersona")  // de esta tabla Factura de la base de datos
private Persona fk_IdPersona;     //  viene de la clase persona

@Column(name="facturaVenta")
private String facturaVenta;

@Column(name="valorTotalFct")
private float valorTotalFct;

@Column(name="fechafactura")
private Date fechaFactura;

  // METODO GETTER Y SETTER

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Persona getFk_IdPersona() {
        return fk_IdPersona;
    }

    public void setFk_IdPersona(Persona fk_IdPersona) {
        this.fk_IdPersona = fk_IdPersona;
    }

    public String getFacturaVenta() {
        return facturaVenta;
    }

    public void setFacturaVenta(String facturaVenta) {
        this.facturaVenta = facturaVenta;
    }

    public float getValorTotalFct() {
        return valorTotalFct;
    }

    public void setValorTotalFct(float valorTotalFct) {
        this.valorTotalFct = valorTotalFct;
    }

    public Date getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

} 