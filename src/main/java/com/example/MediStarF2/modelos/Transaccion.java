/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.MediStarF2.modelos;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * farmaciaVirtual MediStartPharmacy
 *
 * @author Carlos Alberto Sánchez Medina
 */
// esta entidad siempre se deja pegada al nombre de la clase
// esto se necesita para indicar que la clase Transaccion es una entidad  ...*** con la tabla "categorias" // 
@Entity
@Table(name="transacciones")
public class Transaccion {
// autowire permite inyectar codigo Sql en java

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name="IdTransaccion")
private Integer IdTransaccion;

@ManyToOne
@JoinColumn(name="Fk_IdFactura")
private Factura Fk_IdFactura;   // foreign key ???*****

@ManyToOne
@JoinColumn(name="Fk_IdProducto")
private Producto Fk_IdProducto;

@Column(name="CantidadSalida")
private int CantidadSalida;

@Column(name="ValorUnitarioVnt")
private float ValorUnitarioVnt;
    

   

    public int getIdTransaccion() {
        return IdTransaccion;
    }

    public void setIdTransaccion(int IdTransaccion) {
        this.IdTransaccion = IdTransaccion;
    }

    public Factura getFk_IdFactura() {
        return Fk_IdFactura;
    }

    public void setFk_IdFactura(Factura Fk_IdFactura) {
        this.Fk_IdFactura = Fk_IdFactura;
    }

    public Producto getFk_IdProducto() {
        return Fk_IdProducto;
    }

    public void setFk_IdProducto(Producto Fk_IdProducto) {
        this.Fk_IdProducto = Fk_IdProducto;
    }

    public int getCantidadSalida() {
        return CantidadSalida;
    }

    public void setCantidadSalida(int CantidadSalida) {
        this.CantidadSalida = CantidadSalida;
    }

    public float getValorUnitarioVnt() {
        return ValorUnitarioVnt;
    }

    public void setValorUnitarioVnt(float ValorUnitarioVnt) {
        this.ValorUnitarioVnt = ValorUnitarioVnt;
    }
}
   