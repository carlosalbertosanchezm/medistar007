/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.MediStarF2.Services;

import com.example.MediStarF2.modelos.Persona;
import java.util.List;

/**
 *
 * @author LENOVO
 */
public interface PersonaService {
    public Persona save(Persona persona);
    public void delete(Integer id);
    public Persona findById(Integer id);
    public List<Persona> findAll ();

}
