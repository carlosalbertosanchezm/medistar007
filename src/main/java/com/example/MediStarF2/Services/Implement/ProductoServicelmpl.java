/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.MediStarF2.Services.Implement;


import com.example.MediStarF2.Services.ProductoService;
import com.example.MediStarF2.Dao.ProductoDao;
import com.example.MediStarF2.modelos.Producto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author XIOMARA
 */
@Service
public class ProductoServicelmpl implements ProductoService {
   @Autowired
private ProductoDao productoDao;
//INSERTAR
@Override
@Transactional(readOnly=false)
public Producto save(Producto producto) {
return productoDao.save(producto);
}
//ELIMINAR
@Override
@Transactional(readOnly=false)
public void delete(Integer id) {
productoDao.deleteById(id);
}
//CONSULTAR POR ID
@Override
@Transactional(readOnly=true)
public Producto findById(Integer id) {
return productoDao.findById(id). orElse(null);
}
//CONSULTAR TODOS LOS REGISTROS
@Override
@Transactional(readOnly=true)
public List<Producto> findAll() {
return (List<Producto>) productoDao.findAll();
}





}
