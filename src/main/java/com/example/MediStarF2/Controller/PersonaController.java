/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.MediStarF2.Controller;
import com.example.MediStarF2.Services.PersonaService;
import com.example.MediStarF2.modelos.Persona;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Carlos Alberto Sánchez Medina
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/persona")
public class PersonaController {

    @Autowired
    private PersonaService personaservice;

    @PostMapping(value = "/")
    public ResponseEntity<Persona> agregar(@RequestBody Persona persona) {
        Persona obj = personaservice.save(persona);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Persona> eliminar(@PathVariable Integer id) {
        Persona obj = personaservice.findById(id);
        if (obj != null) {
            personaservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Persona> editar(@RequestBody Persona persona) {
        Persona obj = personaservice.findById(persona.getIdPersona());
        if (obj != null) {
            obj.setRolPersona(persona.getRolPersona());
            obj.setNombrePersona(persona.getNombrePersona());
            obj.setApellidoPersona(persona.getApellidoPersona());
            obj.setUsuarioPersona(persona.getUsuarioPersona());
            obj.setClavePersona(persona.getClavePersona());
            obj.setTipoIdentificacion(persona.getTipoIdentificacion());
            obj.setNumeroIdentificacion(persona.getNumeroIdentificacion());

            obj.setDireccionPersona(persona.getDireccionPersona());
            obj.setCiudadPersona(persona.getCiudadPersona());
            obj.setEmailPersona(persona.getEmailPersona());
            obj.setTelefonoPersona(persona.getTelefonoPersona());
            obj.setFechaLogging(persona.getFechaLogging());
            personaservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Persona> consultarTodo() {
        return personaservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Persona consultaPorId(@PathVariable Integer id) {
        return personaservice.findById(id);
    }

}
