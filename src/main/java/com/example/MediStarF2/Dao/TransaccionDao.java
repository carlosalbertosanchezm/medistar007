/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.MediStarF2.Dao;

import com.example.MediStarF2.modelos.Transaccion;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author LENOVO
 */
public interface TransaccionDao extends
CrudRepository<Transaccion,Integer> {
    
}

