/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.MediStarF2.Dao;

import com.example.MediStarF2.modelos.Persona;  // importa de la clase Persona en modelos
import org.springframework.data.repository.CrudRepository; 

/**
 *
 * @author LENOVO
 */
public interface PersonaDao extends CrudRepository<Persona,Integer>  {
    
}
